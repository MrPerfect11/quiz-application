import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Questions from './questions/Questions';
import Header from './Navbar';
import '../css/quiz.css';


class Quiz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            questions: [
                {
                    questionText: 'What is the capital of Nepal?',
                    answerOptions: [
                        { answerText: 'Pokhara', isCorrect: false },
                        { answerText: 'Butwal', isCorrect: false },
                        { answerText: 'Kathmandu', isCorrect: true },
                        { answerText: 'Dhankuta', isCorrect: false },
                    ],
                },
                {
                    questionText: 'Who is CEO of Amazon?',
                    answerOptions: [
                        { answerText: 'Jeff Bezos', isCorrect: true },
                        { answerText: 'Elon Musk', isCorrect: false },
                        { answerText: 'Bill Gates', isCorrect: false },
                        { answerText: 'Tony Stark', isCorrect: false },
                    ],
                },
                {
                    questionText: 'The iPhone was created by which company?',
                    answerOptions: [
                        { answerText: 'Apple', isCorrect: true },
                        { answerText: 'Intel', isCorrect: false },
                        { answerText: 'Amazon', isCorrect: false },
                        { answerText: 'Microsoft', isCorrect: false },
                    ],
                },
                {
                    questionText: 'What is the capital of India?',
                    answerOptions: [
                        { answerText: 'Mumbai', isCorrect: false },
                        { answerText: 'Locknow', isCorrect: false },
                        { answerText: 'Amritsar', isCorrect: false },
                        { answerText: 'New Delhi', isCorrect: true },
                    ],
                },
                {
                    questionText: 'Who is known as the light of Asia?',
                    answerOptions: [
                        { answerText: 'Mahatma Gandhi', isCorrect: false },
                        { answerText: 'Dalai Lama', isCorrect: false },
                        { answerText: 'Gautam Buddha', isCorrect: true },
                        { answerText: 'Maha Rana Pratap', isCorrect: false },
                    ],
                },
                {
                    questionText: 'Which is the highest peak in the world?',
                    answerOptions: [
                        { answerText: 'Mount Everest', isCorrect: true },
                        { answerText: 'Mount K2', isCorrect: false },
                        { answerText: 'Mount Kanchanjangha', isCorrect: false },
                        { answerText: 'Mpunt Manaslu', isCorrect: false },
                    ],
                },
                {
                    questionText: 'Which is the largest continent in the world?',
                    answerOptions: [
                        { answerText: 'Australia', isCorrect: false },
                        { answerText: 'Asia', isCorrect: true },
                        { answerText: 'Europe', isCorrect: false },
                        { answerText: 'North America', isCorrect: false },
                    ],
                },
                {
                    questionText: 'How many months are there in a year?',
                    answerOptions: [
                        { answerText: '9', isCorrect: false },
                        { answerText: '10', isCorrect: false },
                        { answerText: '11', isCorrect: false },
                        { answerText: '12', isCorrect: true },
                    ],
                },
                {
                    questionText: 'How many minutes are there in an hour?',
                    answerOptions: [
                        { answerText: '61', isCorrect: false },
                        { answerText: '60', isCorrect: true },
                        { answerText: '59', isCorrect: false },
                        { answerText: '58', isCorrect: false },
                    ],
                },
                {
                    questionText: 'How many days are there in a week?',
                    answerOptions: [
                        { answerText: '1', isCorrect: false },
                        { answerText: '4', isCorrect: false },
                        { answerText: '6', isCorrect: false },
                        { answerText: '7', isCorrect: true },
                    ],
                },
            ],
        };
    }

    randomizeQuestions = () => {
        this.state.questions.sort(() => Math.random() - 0.5);
    };

    randomizeAnswer = () => {
        this.state.questions.map(q => {
            q.answerOptions.sort(() => Math.random() - 0.5)
        });
    };

    render() {
        const { questions } = this.state;
        questions.sort(() => Math.random() - 0.5);
        return (
            <>
                <Header />
                <div className='wrapper'>
                    <Questions
                        questions={questions}
                        randomizeQuestions={this.randomizeQuestions}
                        randomizeAnswer={this.randomizeAnswer}
                    />
                </div>
            </>
        );
    }
}

export default withRouter(Quiz);
