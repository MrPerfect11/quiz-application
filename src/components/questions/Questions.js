import React, { Component } from "react";

class Questions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentQuestion: 0,
      showScore: false,
      score: 0,
      answerChoose: false,
      submit: true,
      answerSelected: false,
      selected: {},
      myStyle: { color: 'yellow' }
    };
  }

  handleAnswerOptionClick = (isCorrect) => {
    let res = this.state.score + 1;
    if (isCorrect) {
      this.setState({ ...this.state, score: res });
    }

    let nextQuestion = this.state.currentQuestion + 1;
    console.log("stat", nextQuestion);

    if (nextQuestion < this.props.questions.length) {
      this.setState({ currentQuestion: nextQuestion });
    } else {
      this.setState({
        showScore: true,
      });
    }
  };

  handleReset = () => {
    this.setState({
      currentQuestion: 0,
      showScore: false,
      score: null,
      answerChoose: false,
      submit: true,
      answerSelected: false,
      selected: {},
      myStyle: { color: 'yellow' }
    });
    console.log(this.state.score);
    this.props.randomizeQuestions();
    this.props.randomizeAnswer();
  };

  handleAnswerClick = (answer, index) => {

    this.setState({
      answerChoose: answer.isCorrect,
      answerSelected: true,
      selected: answer
    })
  }

  handleSubmit = () => {
    let isCorrect = this.state.answerChoose
    let res = this.state.score + 1;
    if (isCorrect) {
      this.setState({ ...this.state, score: res });
    }
    let nextQuestion = this.state.currentQuestion + 1;
    console.log("stat", nextQuestion);

    if (nextQuestion < this.props.questions.length) {
      this.setState({ submit: false })
    }
    this.setState({ submit: false });
    let currentState = this.state;
    if (this.state.selected.isCorrect) {
      console.log('border')
      this.setState({ currentState, myStyle: { ...this.state.myStyle, borderColor: 'green' } })
    } else {
      this.setState({ currentState, myStyle: { ...this.state.myStyle, borderColor: 'red' } })
    }

  }

  handleNext = () => {
    this.setState({
      answerSelected: false
    })
    let nextQuestion = this.state.currentQuestion + 1;
    console.log("stat", nextQuestion);

    if (nextQuestion < this.props.questions.length) {
      this.setState({ currentQuestion: nextQuestion });
    }
    else {
      this.setState({
        showScore: true,
      });
    }
    this.setState({ submit: true, myStyle: { color: 'yellow' } })
  }

  render() {
    const { currentQuestion, showScore, score } = this.state;
    const { questions } = this.props;
    return (
      <div className="quiz-card">
        {showScore ? (
          <div className="score-section">
            You scored {score} out of {questions.length} &nbsp;
            <button className="btn btn-success" onClick={this.handleReset}>Play Again</button>
          </div>
        ) : (
          <div className="question-section">
            {console.log(currentQuestion)}
            <div className="question-count">
              <span>Question {currentQuestion + 1}</span>/{questions.length}
            </div>
            <div className="question-text">
              {questions[currentQuestion].questionText}
            </div>
            <div className="answer-section">
              {questions[currentQuestion].answerOptions.map(
                (answerOption, index) => (
                  <button className={!this.state.submit && answerOption.isCorrect ? 'answersCorrect' : 'answers'} style={answerOption === this.state.selected ? this.state.myStyle : {}}
                    key={index}
                    onClick={() =>
                      this.handleAnswerClick(answerOption, index)
                    }

                  >
                    {answerOption.answerText}
                  </button>
                )
              )}
            </div>
            {this.state.submit ? <button disabled={!this.state.answerSelected} className="btn btn-success float-right" onClick={this.handleSubmit}>Submit</button> : <button className="btn btn-primary float-right" onClick={this.handleNext}>{console.log(this.state.currentQuestion + 1, this.props.questions.length)}{this.state.currentQuestion + 1 < this.props.questions.length ? 'Next' : 'Finish'}</button>}
          </div>
        )}
      </div>
    );
  }
}

export default Questions;
