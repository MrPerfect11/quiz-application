import React, { Component } from 'react';
import { Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class Header extends Component {
    handleClick = e => {
        localStorage.removeItem('user');
    }
    render() {
        const user = JSON.parse(localStorage.getItem('user'));
        console.log('this', user);
        return (
            <Navbar bg="light">
                <Navbar.Brand>Quiz Application</Navbar.Brand>
                <Nav className="ml-auto">
                    {
                        user && user.email ?

                            <Nav>
                                <span style={{ padding: '.5rem .1rem' }}>Welcome</span>
                                <NavDropdown title={user && user.email}>
                                    <NavDropdown.Item as={Link} to='/' onClick={this.handleClick}>Logout</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                            :
                            <>
                                <Link to='/' className="pr-5">Login</Link>
                                <Link to='/register' className="pr-5">Sign Up</Link>
                            </>
                    }
                </Nav>
            </Navbar>
        )
    }
}

export default Header;