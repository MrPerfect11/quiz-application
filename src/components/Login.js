import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Header from './Navbar';


const emailRegexp = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    Object.values(rest).forEach(val => {
        val === null && (valid = false);
    });

    return valid;
};

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null,
            formErrors: {
                email: ''
            }
        };
    }
    handleSubmit = e => {
        e.preventDefault();
        const usersArray = JSON.parse(localStorage.getItem('users'));

        console.log(usersArray);
        if (formValid(this.state)) {
            if (usersArray == null) {
                alert('Invalid Email or Password');
            }
            else {
                const match = usersArray.some(user => user.email === this.state.email && user.password === this.state.password);
                if (match) {
                    localStorage.setItem('user', JSON.stringify({ email: this.state.email, password: this.state.password }));
                    this.props.history.push('/quiz');
                    console.log('Hamro', this.props.history);
                }
                else {
                    alert("Email and Password Doesnot Match");
                }
            }
        }
        else {
            alert('Invalid Email or Password');
        }
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
        let formErrors = this.state.formErrors;
        if (name === 'email') {
            formErrors.email = !emailRegexp.test(value) ? 'Invalid Email Address' : '';
            console.log(formErrors.email);
            this.setState({ formErrors });
        }
    }
    render() {
        const { formErrors } = this.state;
        return (
            <div>
                <Header />
                <div className="wrapper">
                    <div className="form-wrapper">
                        <h1>Login</h1>
                        <form onSubmit={this.handleSubmit} noValidate>
                            <div className="email">
                                <label htmlFor="email">Email</label>
                                <input className={formErrors.email.length > 0 ? 'error' : null} type="text" placeholder="Email" name="email" onChange={this.handleChange} />
                                {formErrors.email.length > 0 && (
                                    <span className='errorMessage'>{formErrors.email}</span>
                                )}
                            </div>
                            <div className="password">
                                <label htmlFor="password">Password</label>
                                <input type="password" placeholder="Password" name="password" onChange={this.handleChange} />
                            </div>
                            <div className="createAccount">
                                <button type="submit">Login</button>
                                <div>
                                    <small>Don't have an account?</small><Link to='/register'>Sign Up</Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Login);