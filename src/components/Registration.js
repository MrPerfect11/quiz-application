import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import Header from './Navbar';


const emailRegexp = RegExp(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);

const formValid = ({ formErrors, ...rest }) => {
    let valid = true;

    Object.values(formErrors).forEach(val => {
        val.length > 0 && (valid = false);
    });

    Object.values(rest).forEach(val => {
        val === null && (valid = false);
    });

    return valid;
};

class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: null,
            lastName: null,
            email: null,
            password: null,
            confirmPassword: null,
            formErrors: {
                firstName: '',
                lastName: '',
                email: '',
                password: '',
                confirmPassword: ''
            }
        };
    }
    handleSubmit = e => {
        e.preventDefault();
        let usersArray = [];
        if (formValid(this.state)) {
            alert('Account Created Successfully');
            this.props.history.push('/');
            usersArray = JSON.parse(localStorage.getItem('users'));
            if (usersArray && usersArray.length > 0) {
                usersArray.push({ email: this.state.email, password: this.state.password });
                localStorage.setItem('users', JSON.stringify(usersArray))
            } else {
                usersArray = []
                usersArray.push({ email: this.state.email, password: this.state.password });
                localStorage.setItem('users', JSON.stringify(usersArray))
            }
        } else {
            alert('Invalid Form');
        }
    };
    handleChange = e => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value });
        let formErrors = this.state.formErrors;
        switch (name) {
            case 'firstName':
                formErrors.firstName = value.length < 3 ? 'Minimum 3 Characters Required' : '';
                break;
            case 'lastName':
                formErrors.lastName = value.length < 3 ? 'Minimum 3 Characters Required' : '';
                break;
            case 'email':
                formErrors.email = !emailRegexp.test(value) ? 'Invalid Email Address' : '';
                break;
            case 'password':
                formErrors.password = value.length < 6 ? 'Minimum 6 Characters Required' : '';
                break;
            case 'confirmPassword':
                formErrors.confirmPassword = this.state.password === e.target.value ? '' : 'Password does not match';
                break;
            default:
                break;
        }

        this.setState({ formErrors });

    }
    render() {

        const { formErrors } = this.state;
        return (
            <div>
                <Header />
                <div className="wrapper">
                    <div className="form-wrapper">
                        <h1>Create Account</h1>
                        <form onSubmit={this.handleSubmit} noValidate>
                            <div className="firstName">
                                <label htmlFor="firstName">First Name</label>
                                <input className={formErrors.firstName.length > 0 ? 'error' : null} type="text" placeholder="First Name" name="firstName" onChange={this.handleChange} />
                                {formErrors.firstName.length > 0 && (
                                    <span className='errorMessage'>{formErrors.firstName}</span>
                                )}
                            </div>
                            <div className="lastName">
                                <label htmlFor="lastName">Last Name</label>
                                <input className={formErrors.lastName.length > 0 ? 'error' : null} type="text" placeholder="Last Name" name="lastName" onChange={this.handleChange} />
                                {formErrors.lastName.length > 0 && (
                                    <span className='errorMessage'>{formErrors.lastName}</span>
                                )}
                            </div>
                            <div className="email">
                                <label htmlFor="email">Email</label>
                                <input className={formErrors.email.length > 0 ? 'error' : null} type="email" placeholder="Email" name="email" onChange={this.handleChange} />
                                {formErrors.email.length > 0 && (
                                    <span className='errorMessage'>{formErrors.email}</span>
                                )}
                            </div>
                            <div className="password">
                                <label htmlFor="password">Password</label>
                                <input className={formErrors.password.length > 0 ? 'error' : null} type="password" placeholder="Password" name="password" onChange={this.handleChange} />
                                {formErrors.password.length > 0 && (
                                    <span className='errorMessage'>{formErrors.password}</span>
                                )}
                            </div>
                            <div className="confirmPassword">
                                <label htmlFor="confirmPassword">Confirm Password</label>
                                <input className={formErrors.confirmPassword.length > 0 ? 'error' : null} type="password" placeholder="Confirm Password" name="confirmPassword" onChange={this.handleChange} />
                                {formErrors.confirmPassword.length > 0 && (
                                    <span className='errorMessage'>{formErrors.confirmPassword}</span>
                                )}
                            </div>
                            <div className="createAccount">
                                <button type="submit">Create Account</button>
                                <div>
                                    <small>Already have an account?</small> <Link to='/'>Login</Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(Registration);