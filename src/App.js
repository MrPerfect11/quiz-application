import './App.css';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import Registration from './components/Registration'
import Login from './components/Login'
import Quiz from './components/Quiz'
import Protected from './components/Protected'

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path='/' exact component={Login}>
          <Protected Cmp={Login} />
        </Route>
        <Route path='/register' component={Registration}>
          <Protected Cmp={Registration} />
        </Route>
        <Route path='/quiz' component={Quiz}>
          <Protected Cmp={Quiz} />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
